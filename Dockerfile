FROM python:3.8-buster

WORKDIR /home/metrics
COPY . /home/metrics

RUN python3 -m pip install -r requirements.txt

EXPOSE 8923

CMD ["python3", "monitor.py"]
