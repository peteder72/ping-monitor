# Ping monitor with Prometheus export

Program monitors ping to certain addresses and exports them via a simple HTTP server, talking *Prometheus*

## Build and run

```bash
python3 -m pip install requirements.txt
sudo python3 monitor.py
```

Script can also run in a Docker container:

```bash
docker-compose build
docker-compose up
```

## Configuration

To configure, create a file named *servers.env* and add comma-separated hosts

```bash
MONITOR_SERVERS=127.0.0.1,localhost
```