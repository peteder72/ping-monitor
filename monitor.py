import os
from time import sleep

import click
import dotenv
from prometheus_client import Gauge, start_http_server
from pythonping import ping

addresses = [
    'google.com'
]

metric = Gauge(
    "ping_ms",
    "Ping time",
    ['addr']
)


@click.command()
@click.option('-i', '--interval', type=int, default=10, show_default=True)
@click.option('-p', '--port', type=int, default=8923, show_default=True)
@click.option('-m', '--max-number', type=int, default=9999, show_default=True)
@click.option('-v', '--verbose', count=True)
def monitor(interval, port, max_number, verbose):

    dotenv.load_dotenv('servers.env')
    add_serv = os.environ.get('MONITOR_SERVERS')

    if add_serv:
        if verbose > 0:
            print("[*] Loading servers from env")
        addresses.extend(add_serv.split(','))
    elif verbose > 0:
        print("[!] No servers were loaded")

    start_http_server(port)
    if verbose > 0:
        print("[+] HTTP server started")

    while True:

        for addr in addresses:
            if verbose > 1:
                print(f"[*] Pinging \"{addr}\"")
            res = ping(addr)

            val = res.rtt_avg_ms if res.success() else max_number

            if verbose > 1:
                print(f"[+] Got result {val}")

            metric.labels(addr=addr).set(val)

        if verbose > 1:
            print("[*] Sleeping")

        sleep(interval)


if __name__ == '__main__':

    # Pylint and click are not friends
    monitor()   # pylint: disable=no-value-for-parameter
